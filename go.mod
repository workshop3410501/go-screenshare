module lh-toolkit

go 1.22.3

require (
	gioui.org v0.6.0
	github.com/gorilla/websocket v1.5.1
	github.com/kbinani/screenshot v0.0.0-20230812210009-b87d31814237
)

require (
	gioui.org/cpu v0.0.0-20220412190645-f1e9e8c3b1f7 // indirect
	gioui.org/shader v1.0.8 // indirect
	github.com/gen2brain/shm v0.1.0 // indirect
	github.com/go-text/typesetting v0.1.1 // indirect
	github.com/jezek/xgb v1.1.1 // indirect
	github.com/lxn/win v0.0.0-20210218163916-a377121e959e // indirect
	golang.org/x/exp v0.0.0-20240506185415-9bf2ced13842 // indirect
	golang.org/x/exp/shiny v0.0.0-20240506185415-9bf2ced13842 // indirect
	golang.org/x/image v0.16.0 // indirect
	golang.org/x/net v0.25.0 // indirect
	golang.org/x/sys v0.20.0 // indirect
	golang.org/x/text v0.15.0 // indirect
)
